#include <QList>
#include <QDebug>
#include <QCoreApplication>

class UserInfo
{
public:
    UserInfo(QString key,QString value){
        this->key = key;
        this->value = value;
    }
public:
    QString key;
    QString value;
};

QList<UserInfo*>datalist;
int main()
{
    //对数据操作
    datalist.append(new UserInfo("a", "192.168.1.1"));
    datalist.append(new UserInfo("b", "192.168.1.2"));
    datalist.append(new UserInfo("c", "192.168.1.3"));
    datalist.append(new UserInfo("d", "192.168.1.4"));
    qDebug()<<"第一次遍历";
    for(int i = 0;i < datalist.size();++i)
    {
        qDebug()<<datalist[i]->key <<datalist[i]->value;
    }
    //遍历
    for(int i = 0;i < datalist.size();++i)
    {
        if(datalist[i]->key == "c")
            delete datalist.takeAt(i);
    }
    qDebug()<<"第二次遍历";
    for(int i = 0;i < datalist.size();++i)
    {
        qDebug()<<datalist[i]->key <<datalist[i]->value;
    }
}




