#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QKeyEvent>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

// 键盘按下事件
void MainWindow::keyPressEvent(QKeyEvent *event)
{
    // 是否按下Ctrl键
    if(event->modifiers() == Qt::ControlModifier){

        // 是否按下M键
        if(event->key() == Qt::Key_M)
            // 窗口最大化
            setWindowState(Qt::WindowMaximized);
    }
    else if(event->key() == Qt::Key_Q)
        ui->label->setText("按下Q键");
    else if(event->key() == Qt::Key_W)
        ui->label->setText("按下W键");
    else if(event->key() == Qt::Key_E)
        ui->label->setText("按下E键");
    else if(event->key() == Qt::Key_R)
        ui->label->setText("按下R键");
    else QWidget::keyPressEvent(event);
}

// 按键释放事件
void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    //其他操作
}
